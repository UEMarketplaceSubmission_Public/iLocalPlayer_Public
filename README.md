# 1. iLocalPlyer UE4 Plugin    

**Please download the "iLocalPlayer" UE4 Plugin from the UE4 Marketplace: [https://www.unrealengine.com/marketplace/ilocalplayer](https://www.unrealengine.com/marketplace/ilocalplayer)**  

"iLocalPlayer" UE4 Plugin contains following advanced functions.    

1.	**Side-by-Side Stereo 3D implementing**:    
You can get two eye images on your UE4 game viewport.The left eye image is on the left side of the viewport and right eye image is on the right side.    
2. **Asymmetric projection(or named asymmetric view frustum) implementing**:    
With this function, you can implement asymmetric projection matrix effects. Such as splicing one spherical screen with multi-PCs(Multi UE4 Game Viewports).

**Notice:** We submitted a new Pull-Request into "EpicGames/UnrealEngine:master" to fix the issue(When capturing a movie in the sequencer, Side-by-Side Stereo-3D effect is not correct). The Pull-Request Link: [https://github.com/EpicGames/UnrealEngine/pull/4370](https://github.com/EpicGames/UnrealEngine/pull/4370 "https://github.com/EpicGames/UnrealEngine/pull/4370")

## 1.1 "iLocalPlayer" UE4 Plugin Demo    

The "iLocalPlayerUE4" Project is a UE4 Project Demo for "iLocalPlayer" UE4 Plugin.

### 1.1.1 Video Demo    
YouTube:    
[iLocalPlayer UE4 Plugin Demo](https://www.youtube.com/watch?v=oAbrCDQswLo&feature=youtu.be)    
or        
YouKu:    
[iLocalPlayer UE4 Plugin Demo](http://v.youku.com/v_show/id_XMjY3NzE4ODUxMg==.html)

### 1.1.2 UE4 Game App Demos
**iLocalPlayerUE4Demo_01**: Demonstrate the "Side-by-Side Stereo 3D" and the "Asymmetric View Frustum" functions.    
**Path**:[https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/blob/master/Demos/iLocalPlayerUE4Demo_01.zip](https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/blob/master/Demos/iLocalPlayerUE4Demo_01.zip "https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/blob/master/Demos/iLocalPlayerUE4Demo_01.zip")    

ScreenShot by using "F9" Key:    

![iLocalPlayerUE4Demo_01 01_F9_01](README.md.resources/01_Images/02_Demo/iLocalPlayerUE4Demo_01/01_F9_01.png)

ScreenShot by using "shot" console command:   

![iLocalPlayerUE4Demo_01 02_Shot_01](README.md.resources/01_Images/02_Demo/iLocalPlayerUE4Demo_01/02_Shot_01.png)

**iLocalPlayerUE4Demo_02**: Demonstrate the "Side-by-Side Stereo 3D" function and the "Asymmetric View Frustum"  function with playing sequencer.    
**Path**:[https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/blob/master/Demos/iLocalPlayerUE4Demo_02.zip](https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/blob/master/Demos/iLocalPlayerUE4Demo_02.zip "https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/blob/master/Demos/iLocalPlayerUE4Demo_02.zip")    

ScreenShot by using "F9" Key:    

![iLocalPlayerUE4Demo_02 01_F9_01](README.md.resources/01_Images/02_Demo/iLocalPlayerUE4Demo_02/01_F9_01.png)

ScreenShot by using "shot" console command:   

![iLocalPlayerUE4Demo_02 02_Shot_01](README.md.resources/01_Images/02_Demo/iLocalPlayerUE4Demo_02/02_Shot_01.png)


## 1.2 Config for your "iLocalPlayer" UE4 Plugin   

The config file of "iLocalPlayer" UE4 Plugin is named "iLocalPlayerUE4Plugin.ini", and it's file path is:
`<YourUE4Project>/Plugins/iLocalPlayerUE4Plugin/Config/iLocalPlayerUE4Plugin.ini`

The content of "iLocalPlayerUE4Plugin.ini" is something like this:

```
[iLocalPlayer.CustomProjectionData]
Enable=True
LeftClippingAngle=-37.312466
RightClippingAngle=44.751022
BottomClippingAngle=-15.000000
TopClippingAngle=41.062759

[iLocalPlayer.Stereo3D]
InverseLeftEyeAndRightEye=True
ScaleOfLeftEyeAndRightEyeDistance=-1.250000
StereoShowType=2
```

## 1.3 How to use the "iLocalPlayer" Plugin
### 1.3.1 Enable the "iLocalPlayer" Plugin in the "Plugins" window of your project's UE4 Editor.

![000_Enable_iLocalPlayer_Plugin_01](README.md.resources/01_Images/03_Enable_iLocalPlayer_Plugin/000_Enable_iLocalPlayer_Plugin_01.png)

### 1.3.2 Set the "Local Player Class"     
There are three ways to set the "Local Player Class":     
#### 1.3.2.1 The first way：
Set the value of "Local Player Class"(It's in the "Project Settings->General Settings->Default Classes" panel of your project's UE4 Editor) to "iLocalPlayer".    

![000_Enable_iLocalPlayer_Plugin_01](README.md.resources/01_Images/03_Enable_iLocalPlayer_Plugin/001_Enable_iLocalPlayer_Plugin_01.png)

#### 1.3.2.2 The second way：
Change the value of "I Local Player Class" variable to "iLocalPlayer" in the Actor Blueprint of `"BPA_iLocalPlayerInfoDisplayer"` Actor. More detail infos can be found in the Demo Project "iLocalPlayerUE4.uproject". There is a Actor named `"BPA_iLocalPlayerInfoDisplayer"` in the level.

You can enable our "iLocalPlayerUE4Plugin" plugin and change the projection data simply by using the blueprint nodes shown in this image.    

![003_BPA_iLocalPlayerInfoDisplayer_02](README.md.resources/01_Images/03_Enable_iLocalPlayer_Plugin/003_BPA_iLocalPlayerInfoDisplayer_02.PNG)


The more comprehensive pictures are below:    

![003_BPA_iLocalPlayerInfoDisplayer_00](README.md.resources/01_Images/03_Enable_iLocalPlayer_Plugin/003_BPA_iLocalPlayerInfoDisplayer_00.PNG)

![003_BPA_iLocalPlayerInfoDisplayer_01](README.md.resources/01_Images/03_Enable_iLocalPlayer_Plugin/003_BPA_iLocalPlayerInfoDisplayer_01.PNG)

![003_BPA_iLocalPlayerInfoDisplayer_03](README.md.resources/01_Images/03_Enable_iLocalPlayer_Plugin/003_BPA_iLocalPlayerInfoDisplayer_03.PNG)

#### 1.3.2.3 The third way：

The demo named `"iLP_CPlusPlus_Demo"` shows you how to use the CPP code of "iLocalPlayerUE4Plugin" plugin in your C++ UE4 Project.

You can use the following code to call the C++ functions of "iLocalPlayerUE4Plugin" plugin(File: `iLP_CPlusPlus_Demo\Source\iLP_CPlusPlus_Demo\iLocalPlayer_Test_Actor.cpp`)

    #include "iLocalPlayer_Test_Actor.h"
    #include "UObject/Object.h"
    #include "iLocalPlayer.h"
    #include "iLocalPlayerBlueprintFunctionLibrary.h"
    
    // Sets default values
    AiLocalPlayer_Test_Actor::AiLocalPlayer_Test_Actor()
    {
     	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    	PrimaryActorTick.bCanEverTick = true;
    
    }
    
    // Called when the game starts or when spawned
    void AiLocalPlayer_Test_Actor::BeginPlay()
    {
    	Super::BeginPlay();
    
    	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	// Enable iLocalPlayer Plugin.
    	ULocalPlayer* LocalPlayerRef=nullptr;
    	int32 NumOfLocalPlayers = 1;
    	UiLocalPlayerBlueprintFunctionLibrary::GetNumOfLocalPlayers(this, NumOfLocalPlayers);
    	UiLocalPlayerBlueprintFunctionLibrary::CreateLocalPlayer(this, UiLocalPlayer::StaticClass(), NumOfLocalPlayers-1, false, 0, LocalPlayerRef);
    	UiLocalPlayer* iLocalPlayerRef = (UiLocalPlayer*)LocalPlayerRef;
    	// Change the clipping angles of the perspective projection.
    	iLocalPlayerRef->CustomProjectionData.Enable = true;
    	iLocalPlayerRef->CustomProjectionData.LeftClippingAngle = -15.0f;
    	iLocalPlayerRef->CustomProjectionData.RightClippingAngle = 75.0f;
    	iLocalPlayerRef->CustomProjectionData.BottomClippingAngle = -30.0f;
    	iLocalPlayerRef->CustomProjectionData.TopClippingAngle = 30.0f;
    	// Change the parameter value of side-by-side stereo 3D.
    	iLocalPlayerRef->StereoShowType = EStereoShowType::TwoEyes;
    	iLocalPlayerRef->InverseLeftEyeAndRightEyeForSideBySideStereo3D = true;
    	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    
    // Called every frame
    void AiLocalPlayer_Test_Actor::Tick(float DeltaTime)
    {
    	Super::Tick(DeltaTime);
    
    }

> Note: If you are using the "iLocalPlayerUE4Plugin" plugin version that did not export(some thing like "`__declspec(dllexport)`") the "UiLocalPlayer" and "UiLocalPlayerBlueprintFunctionLibrary" classes, you will need to export the two classes at the place the two classes are declared. First, you need to copy the entire "iLocalPlayerUE4Plugin" plugin into the "Plugins" folder of your UE4 C++ project . Second, you should find the "UiLocalPlayer" and "UiLocalPlayerBlueprintFunctionLibrary" classes. Add the "ILOCALPLAYERUE4PLUGIN_API" macro(UE4 generates the macro automatically, so don't have to declare that anywhere.). Reference link:[https://wiki.unrealengine.com/Plugins,_How_To_Use_CPP_Code_Of_Plugin_In_Your_Project](https://wiki.unrealengine.com/Plugins,_How_To_Use_CPP_Code_Of_Plugin_In_Your_Project)

In the "iLocalPlayer.h" file("`iLP_CPlusPlus_Demo\Plugins\iLocalPlayerUE4Plugin\Source\iLocalPlayerUE4Plugin\Public\iLocalPlayer.h`"), the declaration of "UiLocalPlayer" class should be like this:

    UCLASS(ClassGroup = "YeHaike|iLocalPlayer", BlueprintType, Blueprintable, meta = (ShortTooltip = "iLocalPlayer is use to modify projection matrix."))
    class ILOCALPLAYERUE4PLUGIN_API UiLocalPlayer : public ULocalPlayer, public iPluginConfig
    {
    	GENERATED_UCLASS_BODY()

In the "iLocalPlayerBlueprintFunctionLibrary.h" file("`iLP_CPlusPlus_Demo\Plugins\iLocalPlayerUE4Plugin\Source\iLocalPlayerUE4Plugin\Public\iLocalPlayerBlueprintFunctionLibrary.h`"), the declaration of "UiLocalPlayerBlueprintFunctionLibrary" class should be like this:

    UCLASS()
    class ILOCALPLAYERUE4PLUGIN_API UiLocalPlayerBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
    {
    
    	GENERATED_BODY()


In your UE4 C++ project(assuming the project name is "`iLP_CPlusPlus_Demo`"), you need to add the "`iLocalPlayerUE4Plugin`" string into the "`PublicDependencyModuleNames`" array in the "`iLP_CPlusPlus_Demo.Build.cs`" file(File path:"`iLP_CPlusPlus_Demo\Source\iLP_CPlusPlus_Demo\iLP_CPlusPlus_Demo.Build.cs`"):

    using UnrealBuildTool;
    
    public class iLP_CPlusPlus_Demo : ModuleRules
    {
    	public iLP_CPlusPlus_Demo(ReadOnlyTargetRules Target) : base(Target)
    	{
    		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
    
    		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "iLocalPlayerUE4Plugin" });
    
    		PrivateDependencyModuleNames.AddRange(new string[] {  });
    	}
    }



### 1.3.3 Enable the "Side-by-Side Stereo 3D"    
To enable the "Side-by-Side Stereo 3D", you just need to do like this: Launch your UE4 Game with the "-emulatestereo" launch command.    
### 1.3.3.1 First way:
Launch your packaged game with a ".bat" file which contains commands like this "`iLocalPlayerUE4.exe  -emulatestereo`".   

![000_Enable_iLocalPlayer_Plugin_01](README.md.resources/01_Images/04_Enable_Side-by-Side_Stereo3D/001_Enable_Side-by-Side_Stereo3D_01.PNG)    

### 1.3.3.2 Second way:    
Launch your unpackaged UE4 project with "Side-by-Side Stereo 3D" enabled.    
First: Create a shortcut of your UE4Engine's "UE4Editor.exe" file.   
Second: Modity the value of the shortcut to something like this "`B:\01_JSKJ\01_Prepare\02_UE4\00_UE4Editor\UE_4.16\Engine\Binaries\Win64\UE4Editor.exe I:\000_Git\01_GitLab\08_UE4MarketplaceSubmission\iLocalPlayer\01_Codes\iLocalPlayerUE4\iLocalPlayerUE4.uproject -emulatestereo -game`".   

![000_Enable_iLocalPlayer_Plugin_01](README.md.resources/01_Images/04_Enable_Side-by-Side_Stereo3D/002_Enable_Side-by-Side_Stereo3D_01.PNG)  

### 1.3.3.3 Third way

**Enable "Side-by-Side Stereo-3D" in the sequencer. And caputure Side-by-Side movie in the sequencer movie capturer**

To enable the Side-by-Side Stereo-3D display effect when you capturing the movie in your sequencer, You should set the value of “Additional Command Line Arguments” to “-emulatestereo” in the “Render Movie Settings” panel. **As shown in the following figtures：**

**Step1： If you need to caputure movie in the sequencer, you should modify the the source code of UE4 Engine**

**Note**: Modify the source code of the UE4 engine downloaded from Github: https://github.com/EpicGames/UnrealEngine/.
Instead of modifying the source code of the UE4 engine downloaded from "EpicGamesLauncher.exe").

We submitted a new Pull-Request into "EpicGames/UnrealEngine:master" to fix the issue(When capturing a movie in the sequencer, Side-by-Side Stereo-3D effect is not correct). The Pull-Request Link(You must be logged into a UE4-Github linked account to view this): https://github.com/EpicGames/UnrealEngine/pull/4370

You can get the source code of the UE4 engine from Github by this way: https://www.unrealengine.com/en-US/ue4-on-github    

About how to compile the source code of UE4 engine downloaded from Github, please see the “README.md” file(https://github.com/EpicGames/UnrealEngine) and the document(https://docs.unrealengine.com/en-us/Programming/Development/BuildingUnrealEngine).


**Step2: Open the "rendering movie Settings" Panel of Sequencer**    

![Open the "rendering movie Settings" Panel of Sequencer](README.md.resources/01_Images/05_Sequencer/001_Sequencer_CaptureMovie.png)


**Step3: Show Advanced Info of "General"**    

![Open the "rendering movie Settings" Panel of Sequencer](README.md.resources/01_Images/05_Sequencer/002_Sequencer_CaptureMovie.png)


**Step4: Enable the "Use Separate Process" property**    

![Open the "rendering movie Settings" Panel of Sequencer](README.md.resources/01_Images/05_Sequencer/003_Sequencer_CaptureMovie.png)


**Step5: Set the value of "Additional Command Line Arguments" property to "-emulatestereo"**    

![Open the "rendering movie Settings" Panel of Sequencer](README.md.resources/01_Images/05_Sequencer/004_Sequencer_CaptureMovie.png)


**Step6: Start Capturing Movie**     

![Open the "rendering movie Settings" Panel of Sequencer](README.md.resources/01_Images/05_Sequencer/005_Sequencer_CaptureMovie.png)

### 1.3.3.4 Fourth way:     

See the following chapter "1.4 View the Side-by-Side stereo 3D result in UE4 Editor".    

### 1.3.3.5 For Sequencer of UE5.3

![image-20231020080225534](README/00_Res/01_Images/image-20231020080225534.png)

## 1.4 View the Side-by-Side stereo 3D result in UE4 Editor

**1. Click the "Advanced Settings...".**

![EnableSide-by-SideModeInEditor_01](README.md.resources/01_Images/01_EnableSide-by-SideModeInEditor/EnableSide-by-SideModeInEditor_01.png)

**2. Set the value of "Additional Launch Parameters": "-emulatestereo".**

![EnableSide-by-SideModeInEditor_02](README.md.resources/01_Images/01_EnableSide-by-SideModeInEditor/EnableSide-by-SideModeInEditor_02.png)

**3. Play your game by clicking the "Standalone Game" button.**  

![EnableSide-by-SideModeInEditor_03](README.md.resources/01_Images/01_EnableSide-by-SideModeInEditor/EnableSide-by-SideModeInEditor_03.png)

**4. View the Side-by-Side Stereo 3D result.**

![EnableSide-by-SideModeInEditor_04](README.md.resources/01_Images/01_EnableSide-by-SideModeInEditor/EnableSide-by-SideModeInEditor_04.png)

## 1.5 Details about "Side-by-Side Stereo Mode"

### 1.5.1 Distance between **LeftEye** and **RightEye**

#### 1.5.1.1 UE4.24 version(iLocalPlayer4UE4 v1.3.2)

Since UE4.24 version(iLocalPlayer4UE4 v1.3.2),
 use the `GetDistanceBetweenLeftEyeAndRightEye()` function of the UiLocalPlayer Object to get the distance between LeftEye and RightEye, and use the `SetDistanceBetweenLeftEyeAndRightEye()` function of the UiLocalPlayer Object to set  the distance between LeftEye and RightEye.

As shown in the following images:

![DistanceBetweenLeftEyeAndRightEye_01](README.md.resources/01_Images/06_DistanceBetweenLeftEyeAndRightEye/01.png)

Reduce the the distance between LeftEye and RightEye:
![DistanceBetweenLeftEyeAndRightEye_02](README.md.resources/01_Images/06_DistanceBetweenLeftEyeAndRightEye/02.png)

Increase the distance between LeftEye and RightEye:
![DistanceBetweenLeftEyeAndRightEye_03](README.md.resources/01_Images/06_DistanceBetweenLeftEyeAndRightEye/03.png)

#### 1.5.1.2 UE4.23 version(iLocalPlayer4UE4 v1.2.2) or older

If the plugin is UE4.23 version(iLocalPlayer4UE4 v1.2.2), use the properties
`ScaleOfLeftEyeAndRightEyeDistance`, `LeftEyeLocationRelativeOffset` and `RightEyeLocationRelativeOffset` of the UiLocalPlayer Object to change the distance between LeftEye and RightEye.

For example, we want to change the distance between LeftEye and RightEye to `16.0f`.
	
In code:
```C++
	iLocalPlayerRef->ScaleOfLeftEyeAndRightEyeDistance = 1.0f;
	iLocalPlayerRef->LeftEyeLocationRelativeOffset = FVector(0.0f, -16.0f / 2.0f, 0.0f);
	iLocalPlayerRef->RightEyeLocationRelativeOffset = FVector(0.0f, 16.0f / 2.0f, 0.0f);
```

Or in blueprint:

Set the `iLocalPlayerRef`.
![DistanceBetweenLeftEyeAndRightEye_04](README.md.resources/01_Images/06_DistanceBetweenLeftEyeAndRightEye/04.png)

Change the distance between LeftEye and RightEye of `iLocalPlayerRef`.
![DistanceBetweenLeftEyeAndRightEye_05](README.md.resources/01_Images/06_DistanceBetweenLeftEyeAndRightEye/05.png)

**Notice:** `LeftEyeLocationRelativeOffset` and `RightEyeLocationRelativeOffset` are `FVector` type. We modified the `Y` values of the vector of `LeftEyeLocationRelativeOffset` and `RightEyeLocationRelativeOffset` for left and right direction.

## 1.6 Hide or Show the Screen Debug Messages

### Console Command: "`DisableAllScreenMessages`"

**Method01:** by "Execude Cosole Command" blueprint Node.

![image-20231020075951872](README/00_Res/01_Images/image-20231020075951872.png)

**Method02:**  by "Tab" key.

![image-20231020075517064](README/00_Res/01_Images/image-20231020075517064.png)

### Console Command: "`EnableAllScreenMessages`"

![image-20231020075706135](README/00_Res/01_Images/image-20231020075706135.png)

## Q&A:

**Q01**: If a Blueprint could handle this function, you wouldn’t need special source code running in the engine baked into the player, right?    
**A01**: Yes, You'd better use the blueprints so that you don't need to modify the engine source code.

**Q02**: Is there no work around how to integrate 3D video (also formatted to SBS) inside a game running iLocalPlayer4UE4?    
**A02**: You can use the media player of UE4 to play your previously recorded Side-by-Side 3D videos, this is very easy. And when using iLocalPlayer4UE4, you can also play that.    

**Q03**: If I had both L and R videos at full res linked in a player to play both at the same time, is there no way to feed these to the GPU to have iLocalPlayer4UE4 then perform the SBS function?    
**A03**: If you want to play Side-by-Side 3D format video, you can use the "Media Player" of UE4 to play the video, and then use the "UMG" of UE4 to display them. Playing video can actually exist with "iLocalPlayer4UE4" at the same time, and playing video is not within the "iLocalPlayer4UE4" function range..    

**Q04**: Whether it may be possible to write a command in the same CutScenePlayer Blueprint used to come out of the game and play these 3D videos to remove the -emulatestereo in the game? I’d need to then re-invoke it when the video ends and returns to the game.    
**A04**: You cannot toggle the "-emulatestereo" command at runtime.  You can use the "Media Player" of UE4 to play the video, and then use the "UMG" of UE4 to display them. This way, when you don't need to play the video, you can remove the UMG widget from the viewport and display the real-time rendering.    

**Q05**: Any plans to allow interaxial to be changed in the future? How involved is that and might you be interested if I could pay you to implement a custom build? I’m a trained stereographer in the live-action arena, wish to modulate the volume of the 3D depending on proximity to subject matter. Thanks for considering.    
**A05**:     
`Date(2019-06-01):`
We will modify our iLocalPlayer4UE4 plugin to support changing the distance between the left eye and right eye at runtime in realtime. But still not sure when it will be completed. But we will finish it as soon as possible.   
`Date(2020-03-07):`
We updated the "iLocalPlayer4UE4" plugin for UE4.24 today, and it will be available on the UE4 marketplace in a few days.
And we updated the document about changing the distance between LeftEye and RightEye:
https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public#12-details-about-side-by-side-stereo-mode

