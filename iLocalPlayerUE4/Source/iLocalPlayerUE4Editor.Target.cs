/*
*  Copyright (c) 2016-2023 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2016/08/09
*
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class iLocalPlayerUE4EditorTarget : TargetRules
{
	public iLocalPlayerUE4EditorTarget(TargetInfo Target) : base(Target)
    {
        bLegacyPublicIncludePaths = false;
        ShadowVariableWarningLevel = WarningLevel.Error;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        bOverrideBuildEnvironment = true;

        Type = TargetType.Editor;
        ExtraModuleNames.AddRange(new string[] { "iLocalPlayerUE4" });
    }
}
