/*
*  Copyright (c) 2016-2023 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2016/08/09
*
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class iLocalPlayerUE4Target : TargetRules
{
	public iLocalPlayerUE4Target(TargetInfo Target) : base(Target)
    {
		Type = TargetType.Game;
        ExtraModuleNames.AddRange(new string[] { "iLocalPlayerUE4" });
    }
}
