/*
*  Copyright (c) 2016-2023 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2016/08/09
*
*/

#include "iLocalPlayerUE4.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, iLocalPlayerUE4, "iLocalPlayerUE4" );
