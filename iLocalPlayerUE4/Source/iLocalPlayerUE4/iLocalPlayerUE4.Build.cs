/*
*  Copyright (c) 2016-2023 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2016/08/09
*
*/

using UnrealBuildTool;

public class iLocalPlayerUE4 : ModuleRules
{
	public iLocalPlayerUE4(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivatePCHHeaderFile = "iLocalPlayerUE4.h";

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
