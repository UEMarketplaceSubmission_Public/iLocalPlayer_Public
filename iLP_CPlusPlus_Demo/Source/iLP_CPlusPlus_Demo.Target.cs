// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class iLP_CPlusPlus_DemoTarget : TargetRules
{
	public iLP_CPlusPlus_DemoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "iLP_CPlusPlus_Demo" } );
	}
}
