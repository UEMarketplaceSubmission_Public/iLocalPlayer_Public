// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class iLP_CPlusPlus_Demo : ModuleRules
{
	public iLP_CPlusPlus_Demo(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "iLocalPlayerUE4Plugin" });

        PrivateDependencyModuleNames.AddRange(new string[] {  });
	}
}
