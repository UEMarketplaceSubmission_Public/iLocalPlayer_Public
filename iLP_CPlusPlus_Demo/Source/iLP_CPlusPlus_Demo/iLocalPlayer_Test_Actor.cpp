// Fill out your copyright notice in the Description page of Project Settings.

#include "iLocalPlayer_Test_Actor.h"
#include "UObject/Object.h"
#include "iLocalPlayer.h"
#include "iLocalPlayerBlueprintFunctionLibrary.h"

// Sets default values
AiLocalPlayer_Test_Actor::AiLocalPlayer_Test_Actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AiLocalPlayer_Test_Actor::BeginPlay()
{
	Super::BeginPlay();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Enable iLocalPlayer Plugin.
	ULocalPlayer* LocalPlayerRef=nullptr;
	int32 NumOfLocalPlayers = 1;
	UiLocalPlayerBlueprintFunctionLibrary::GetNumOfLocalPlayers(this, NumOfLocalPlayers);
	UiLocalPlayerBlueprintFunctionLibrary::CreateLocalPlayer(this, UiLocalPlayer::StaticClass(), NumOfLocalPlayers-1, false, 0, LocalPlayerRef);
	UiLocalPlayer* iLocalPlayerRef = (UiLocalPlayer*)LocalPlayerRef;
	// Change the clipping angles of the perspective projection.
	iLocalPlayerRef->CustomProjectionData.Enable = true;
	iLocalPlayerRef->CustomProjectionData.LeftClippingAngle = -15.0f;
	iLocalPlayerRef->CustomProjectionData.RightClippingAngle = 75.0f;
	iLocalPlayerRef->CustomProjectionData.BottomClippingAngle = -30.0f;
	iLocalPlayerRef->CustomProjectionData.TopClippingAngle = 30.0f;
	// Change the parameter value of side-by-side stereo 3D.
	iLocalPlayerRef->StereoShowType = EStereoShowType::TwoEyes;
	iLocalPlayerRef->InverseLeftEyeAndRightEyeForSideBySideStereo3D = true;
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

// Called every frame
void AiLocalPlayer_Test_Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

