// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class iLP_CPlusPlus_DemoEditorTarget : TargetRules
{
	public iLP_CPlusPlus_DemoEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "iLP_CPlusPlus_Demo" } );
	}
}
