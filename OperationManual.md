# Operation Manual Of "HeadTracking3DVisionUE4"
## Description

**Demo**: ["HeadTracking3DVisionAppDemo(iLocalPlayer)"](https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/blob/master/Demos/HeadTracking3DVisionAppDemo(iLocalPlayer).zip)    
**UE4 Project**: ["HeadTracking3DVisionUE4"](https://gitlab.com/UE4MarketplaceSubmission_Public/iLocalPlayer_Public/tree/master/HeadTracking3DVisionUE4)    

The effect of "HeadTracking3DVisionUE4" is similar to the effect in this video: [3D Head Tracking Webcam OpenCV](https://www.youtube.com/watch?v=h9kPI7_vhAU)    

## Keys
**W**: Move the camera up     
**S**: Move the camera down     
**A**: Move the camera towards the left    
**D**: Move the camera towards the right    
**Q**: Push the camera in    
**E**: Pull the camera out    